/*//////////////////////////////
    Librerias
//////////////////////////////*/
#include "csapp.h"
#include "sbuf.h"
#define FILENAME "lorem.txt"

sbuf_t sp;


/*//////////////////////////////
    Definicion de funciones
//////////////////////////////*/
void *escribir();
void *leer();



/*//////////////////////////////
    Programa principal
//////////////////////////////*/
int main(int argc, char **argv){
    pthread_t productor;
    pthread_t consumidor;
    char *filename;

    if (argc != 2) {
        printf("usage: %s <filename>\n", argv[0]);
        exit(0);
    }
    filename = argv[1];

    sbuf_init(&sp, 10);     // Set the buffer size    

    pthread_create( &productor, NULL, escribir, NULL );
    pthread_create( &consumidor, NULL, leer, NULL );

    pthread_join( &productor, NULL);
    pthread_join( &consumidor, NULL);
    
    return 0;
}



/*/////////////////////////////////
    Implementación de funciones
/////////////////////////////////*/
void *escribir(){
    int fd;
    char c;
    struct stat fileStat;

    if ( stat(FILENAME, &fileStat)<0 ){
        printf(" > Archivo '%s' no fue encontrado.\n", FILENAME);
    }
    else{
        printf(" > Abriendo archivo '%s'...\n", FILENAME);
        fd = Open(FILENAME, O_RDONLY, 0);
        while( Read(fd,&c,1) ){
            usleep(50000);          // Sleeps 50 miliseconds = 50000 microseconds
            sbuf_insert(&sp, c);    // Insert into the buffer
        }
        printf("\n > Leido: %c\n", sbuf_remove(&sp) );
        Close(fd);
    }
    return NULL;
}

void *leer(){
    for (int i = 0; i < 10; i++){
        sleep(1);
        printf("\n > Leido: %c\n", sbuf_remove(&sp) );
    }
    return NULL;
}